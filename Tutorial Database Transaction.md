# Database Transaction

## Pendahuluan

Sudah buat API Save Order kan? Disana terdapat beberapa kegiatan Insert dan Update ke database. Pekan lalu kalian bikin API order tapi lupa belum mengurangi stok yang ada wkwk. Silakan dibuat ya.

Seandainya jika terjadi error didalamnya, misal saat sudah insert ke tabel `orders` ternyata terjadi error pada saat kegiatan selanjutnya alhasil data sudah dimasukkan ke tabel `orders` namun seharusnya order gagal dibuat karena ada error. Pasti akan nyampah di database dengan order order gagal kan? Order tidak terinput ke database saat terjadi error maka kalian pakai fungsi **_Database Transaction_**.

## Pembahasan: Penggunaan Database Transaction

Cara menggunakan DB transaction di laravel ada beberapa cara. kalian coba cara pertama yaa~.

### Menggunakan fungsi `DB::transaction(Closure)`

Penggunaan fungsi `DB::transaction(Closure)` bisa diperhatikan dibawah ya.

```php
use Illuminate\Support\Facades\DB;

//Dibawah ini ada sebuah variabel diluar fungsi DB transaction. Kalian ingin menggunakannya ke dalam DB transaction. Gunakan syntax use() untuk melakukan passing variabel yang berada diluar fungsi callback/closure.

var $sebuahVariabel;

$result = DB::transaction(function () use ($sebuahVariabel) {

    //Bisa bebas lakukan apapun
    $sebuahVariabel->lakukanApapun();
    $sebuahVariabel->sebuahProperti;

    //Kegiatan input atau update ke database, atau bisa juga jika saat ini sudah bisa Save Order tambahkan disini.

    //Jika kalian perlu menambahkan exception didalam sini, bisa ditambahkan dengan `throw Exception` atau bisa juga dengan fungsi handler `abort(<status_code>,<message>)`

    //Setelah melakukan semua kegiatan yang diperlukan, ternyata kalian perlu data hasil dari transaction tersebut (Contoh: data order). Maka kalian tinggal melakukan return dengan variabel atau value apapun yang diperlukan.

    return $dataOrder;

    //Maka value dalam variabel $dataOrder akan diassign ke variabel $result yang ada diatas
}, 5);

//Nilai 5 diatas itu sebagai jumlah percobaan yang dilakukan jika transaksi gagal. Kalau sudah 5 kali gagal maka dia akan melempar exception

```

Apa yang sebenarnya terjadi saat `DB::transaction(Closure)`? Disana kalian hanya menyiapkan transaksi CRUD untuk disimpan secara nyata ke database. Jika terjadi error didalam transaksi, maka transaksi akan di*rollback* dan _Exception_ akan dilempar agar error. Jika ternyata berhasil sampai akhir, maka transaksi akan di*commit*. Pada saat penggunaan `DB::transaction(Closure)` kalian tak perlu secara manual. Dibawah ini ada cara yang manual jika diperlukan.

### Menggunakan fungsi `DB::beginTransaction()`, `DB::rollback()`, dan `DB::commit()`

Mirip dengan fungsi yang sebelumnya tapi kalian akan melakukannya secara manual jika perlu melakukan _Database Transaction_. _Check it out~_

```php
use Illuminate\Support\Facades\DB;

//Mulai transaksi
DB::beginTransaction();

//Kegiatan database apapun yang diperlukan

//Jika ternyata terjadi hal yang salah bisa di rollback
if ($ternyataSalah) {
    DB::rollback();
    abort(404, "Data tidak ditemukan");
}

//Sudah selesai dan tinggal simpan ke database
DB::commit();
```

Itulah tentang _Database Transaction_. Bisa diterapkan pada API Save Order ya. Sekian~.

Referensi:

-   https://laravel.com/docs/9.x/database#database-transactions
