Halo Rekan @Back-End

Karena kemarin kita gak ada materi, agar sesuai target tolong ikuti ya dan pahami tentang Authorization Authentication. Disini kita akan pake `laravel/passport` ya. Referensi nya disini https://laravel.com/docs/9.x/passport#main-content.

Sederhananya kita akses api untuk login nanti dapet `access_token` buat akses route nya. Jadi nanti token tersebut dipakai untuk akses sebuah route yang terlindungi oleh Middleware.

Yang pertama, lakukan command line untuk install package `laravel/passport` ke projek kalian

```
composer require laravel/passport
```

Kemudian lakukan migrate database.

```
php artisan migrate
```

"Kang, kok gak ada di folder migrations?" Migration nya ada, hanya saja dia ada di dalam package `laravel/passport` dan terdaftar otomatis setelah install package. Jadi kita tinggal migrate saja.

Setelah migrate lakukan instalasi personal access client dengan command.

```
php artisan passport:install
```

Command diatas akan menginput 2 credentials ke database lebih tepatnya tabel `oauth_clients`. Satu untuk `client_credentials` satu lagi untuk `password`. Nanti ini dipakai pada saat mendapatkan access token.

Kemudian cari Model `User.php` (itu sudah terdaftar dari awal, jadi tak perlu dibuat lagi). Kalau sudah ketemu ada pemanggilan trait `HasApiTokens` yang ber-namespace `Laravel\Sanctum\HasApiTokens`, silakan ganti jadi pake `Laravel\Passport\HasApiTokens`.

Kemudian cari Config `auth.config` terdapat bagian `guards` silakan tambah config untuk api dengan provider passport.

```php
'guards' => [
    'web' => [
        'driver' => 'session',
        'provider' => 'users',
    ],
    'api' => [
        'driver' => 'passport',
        'provider' => 'users',
    ],
],
```

Nah configurasi sudah selesai, kita akan coba dapetin tokennya. Bikin dulu sebuah akun saja lewat DatabaseSeeder. Cari `database/seeders/DatabaseSeeder.php` disana ada method `run()` didalamnya ada 2 sintaks. Kalian uncomment saja yang dibawah untuk membuat sebuah akun saja, karena yang atas itu bikin 10 akun secara langsung. Lakukan command line.

```
php artisan db:seed --class=DatabaseSeeder
```

Bisa kalian cek di database-nya lewat phpmyadmin harusnya ada row data dengan email yang tertulis pada seeder tersebut. Passwordnya adalah `password` karena sudah dienkripsi jadi di DB tidak bisa dibaca orang.

Sekarang kalian coba tembak api untuk mendapatkan access token tersebut.

URI : `{{host}}/oauth/token`
METHOD : `POST`
REQUEST HEADER :

```json
{
    "Accept": "application/json"
}
```

REQUEST BODY :

```json
{
    "client_id": "<CLIENT_ID>",
    "client_secret": "<CLIENT_SECRET>",
    "username": "<Email yang sudah ada di database>",
    "password": "password",
    "grant_type": "password"
}
```

Catatan:

-   Untuk `CLIENT_ID` dia didapatkan dari command `php artisan passport:install` cari yang merupakan password grant. Biasanya untuk yang password itu id nya 2.
-   Untuk `CLIENT_SECRET` seperti client ID hanya saja pada row yang sama dengan kolom `secret`.

Setelah diakses maka seharusnya kalian akan mendapatkan json dengan struktur:

```json
{
    "token_type": "Bearer",
    "expires_in": 31536000,
    "access_token": "******",
    "refresh_token": "***"
}
```

Kalau masih ada yang tidak sesuai, coba perhatikan lagi langkah - langkahnya. Yow mangats.

Sekarang kita akan mencoba pakai `access_token` yang sudah didapatkan. Caranya coba pada `api.php` buat route seperti ini.

```php
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
```

Cara akses route tersebut pada Postman coba buka tab `Authorization` ubah tipenya `Bearer Token` kemudian paste `access_token` ke `Token`. Setelah route tersebut diakses seharusnya kalian mendapatkan json serupa dengan ini.

```json
{
    "id": 1,
    "name": "Test User",
    "email": "test@example.com",
    "email_verified_at": "2022-09-29T03:46:33.000000Z",
    "created_at": "2022-09-29T03:46:33.000000Z",
    "updated_at": "2022-09-29T03:46:33.000000Z"
}
```

Kalau waktu akses `{{host}}/api/user` malah dapet status `401` berarti ada yang salah dengan tokennya.

Itu untuk password grant. Sekarang silakan coba dengan `client_credentials`. Ini dipakai untuk mengakses route yang tidak memerlukan `username` dan `password` tapi terproteksi dengan `ClientCredentials`.

URI : `{{host}}/oauth/token`
METHOD : `POST`
REQUEST HEADER :

```json
{
    "Accept": "application/json"
}
```

REQUEST BODY :

```json
{
    "client_id": "<CLIENT_ID>",
    "client_secret": "<CLIENT_SECRET>",
    "grant_type": "client_credentials"
}
```

Catatan:

-   Untuk `CLIENT_ID` dia didapatkan dari command `php artisan passport:install` cari yang merupakan client_credentials grant. Biasanya untuk yang password itu id nya 1.
-   Untuk `CLIENT_SECRET` seperti client ID hanya saja pada row yang sama dengan kolom `secret`.

Untuk mengecek apakah token tersebut benar atau salah coba buat route dibawah.

```php
use Laravel\Passport\Http\Middleware\CheckClientCredentials;

Route::middleware(CheckClientCredentials::class)->get('/client_token', function (Request $request) {
    return "client_success";
});
```

Middleware tidak hanya bisa didaftarkan di route, bisa juga di class controller disimpan pada bagian controller seperti dibawah contohnya.

```php
...

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware("auth:api");
    }

...
```

Sekarang kita akan menggunakan fitur `scopes`. Ini dipakai untuk melindungi route agar token hanya bisa mengakses fitur tertentu. Singkatnya mirip seperti `role` dalam aplikasi.

Coba buka `app/Providers/AuthServiceProvider.php`. Pada bagian boot daftarkan `scope` yang akan digunakan.

```php
use Laravel\Passport\Passport;

//Ini untuk mendaftarkan scope
Passport::tokensCan([
    'edit-product' => 'To edit products data',
    'get-product' => 'To retrieve products data',
]);

//Ini untuk mengatur default scope jika saat request bagian scope tidak ditambahkan
Passport::setDefaultScope([
    'get-product',
]);
```

Coba akses lagi `{{host}}/oauth/token` yang `password` seperti diatas, namun tambahkan pada request dengan `scope: "edit-product"`.

Jika mau pakai lebih dari satu scope bisa ditambah dengan separator spasi. Contohnya `scope: "edit-product get-product"`.

Untuk mengecek apakah token dengan scope yang diinginkan pakai middleware `\Laravel\Passport\Http\Middleware\CheckScopes` atau `\Laravel\Passport\Http\Middleware\CheckForAnyScope`.

Coba tambah route untuk mengecek scope.

```php
Route::middleware([
    "auth:api", CheckScopes::class.":edit-product"
])->get('/client_scope', function (Request $request) {
    return "scope_success";
});
```

Route diatas menggunakan 2 middleware, pertama mengecek token yang dikirim apakah sesuai dan selanjutnya mengecek token lagi apakah scope nya sudah sesuai. Jika tidak sesuai dia akan memberikan error dengan status `403`.

Apa bedanya?
Untuk `CheckScopes` dia mengecek semua scope yang terdaftar di middleware. Sedangkan `CheckForAnyScope` itu mengecek setidaknya salah satu scope sesuai.

Untuk lebih lanjut bisa dicek referensi dibawah.

Sekarang kita akan bikin middleware sendiri. Dari tadi middleware apaan ya wkwk. Sederhananya seperti kalau kalian masuk ke mall sebelum masuk kan harus parkir kendaraan dulu, dapet tiket parkir, cek suhu. Di dalem mall bebas ngapain aja. Nah pas beresnya kalian ngambil kendaraan, bayar + ngasih tiket parkir dan pulang. Middleware juga sama, bisa ada proses sebelum controller dipanggil ataupun sesudah controller dipanggil. Sekarang kita akan membuat middleware `IsActive` yaitu mengecek apakah user yang login itu aktif atau tidak.

Pertama kita tambah dulu kolom `is_active` ke tabel `users` dengan default `1`.
Buat middleware seperti biasa dengan penambahan kolomnya seperti ini.

```php
Schema::table('users', function (Blueprint $table) {
    $table->boolean("is_active")->default(1);
});
```

Kemudian kita buat dulu class middleware dengan command

```
php artisan make:middleware IsActive
```

Coba buka ada di `app/Http/Middleware/IsActive.php`

Cara mendapatkan data user itu dengan `$request->user()` seperti diatas atau dengan class `Illuminate\Support\Facades\Auth` caranya `Auth::user()`.

Coba ubah method handle seperti dibawah.

```php
public function handle(Request $request, Closure $next)
{
    $user = $request->user();
    if (!$user->is_active) {
        abort(403, "User is inactive");
    }
    return $next($request);
}
```

Listing code diatas itu untuk mengecek apakah user tidak aktif maka lakukan error dengan status `403` bisa ditambah pesan juga contohnya `User is inactive`.

Cara menambahkan middleware-nya bisa diikuti seperti `CheckScope`

```php
use App\Http\Middleware\IsActive;

Route::middleware(['auth:api', IsActive::class])->get('/user', function (Request $request) {
    return $request->user();
});
```

Coba saja akses route diatas dan set di database langsung `is_active` menjadi `0` maka akan error. Itu contoh sederhananya.

Kalian bisa saja membuat alias untuk sebuah middleware yaitu dengan mendaftarkannya di `app/Http/Kernel.php` pada bagian `$routeMiddleware`. Tambahkan saja nama aliasnya kemudian class dari middleware itu sendiri.

Jika kalian ingin middleware dieksekusi setelah controller tinggal pindahkan pemanggilan fungsi `$next($request)` keatas.

```php
public function handle(Request $request, Closure $next)
{
    $response = $next($request);

    //Suatu proses

    return $response;
}
```

PERLU DIKETAHUI, untuk mengakses `$request->user()` atau `Auth::user()` kalian harus menambahkan middleware `auth:api` sebelumnya. Method `Auth::user()` dapat dipanggil juga dalam controller. Itu bisa digunakan untuk query database misalnya mau mendapatkan data `Order` yang memiliki `user_id` orang yang terlogin.

Begitu deh sekilas tentang Authorization Authentication menggunakan laravel/passport. Kalian bisa dalami lebih lanjut dalam dokumentasinya. Yow mangats semuanyaa~

Referensi:

-   https://laravel.com/docs/9.x/passport#introduction
-   https://laravel.com/docs/9.x/passport#requesting-password-grant-tokens
-   https://laravel.com/docs/9.x/passport#checking-scopes
-   https://laravel.com/docs/9.x/middleware#defining-middleware


Tolong ikuti ya, biar pertemuan yang akan datang kita melangkah lebih lanjut. Jika ada yang ditanyakan, langsung tanyakan disini, karena akang gak akan bahas di pertemuan yang akan datang.

React  jika kalian sudah bisa menjalankan tutorial diatas.