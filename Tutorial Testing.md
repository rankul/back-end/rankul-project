# Testing

## Pendahuluan

Fitur - fitur sudah dibuat, tinggal dicoba saja. Namun kita kalau mau ngecek API nya jalan atau tidak masa harus satu - satu ditembak pake postman? Banyak atuh ya. Apalagi harus ngecek validasi event kekirim, beuh. Nah sekarang kita akan merancang testing untuk projek laravel. Jadi rancangan ini setelah dibangun akan di run dengan sebuah command maka pengujian akan dijalankan. Kita cuma cape sekali saja bikin test dan percayakan padanya jika aplikasi sudah sesuai harapan atau belum. Mau coba dulu?

Coba run commandnya dalam projek ini.

```
php artisan test
```

atau

```
vendor/bin/phpunit
```

## Pembahasan

Sekarang anggap saja projek ini sudah sesuai dengan harapan. Fiturnya sudah cukup, tapi butuh testing nya. Coba perhatikan pada folder `tests`. Disana ada beberapa file dan sebuah folder. Nanti kalian hanya diperbolehkan untuk mengedit file yang ada di dalam folder tersebut.

### Factory Pada Laravel

Pada saat melakukan test kita terkadang perlu melakukan `INSERT` ke database. Masa harus insert secara manual field field nya satu satu. Di Laravel terdapat fitur factory dimana ketika kita mau input ke database tapi kita tidak peduli apapun isinya. Dapat dicek pada folder `database/factories`. Sudah ada Factory disana.

Pada method definition, disana kalian mengatur insert pada database akan diisi dengan apa saja kolomnya. Sama saja seperti create begitu.

Ada yang namanya faker. Apasih faker? jadi itu cuma data dummy saja. Kalian sudah tau `lorem ipsum dolor sit amet ...` itu contohnya teks dummy. Jadi factory ini input ke database dengan data - data dummy. Bisa email, nama orang, alamat dll.

#### Membuat Factory

Untuk membuat factory kalian tinggal melakukan command.

```
php artisan make:factory ProductFactory
```

Maka akan muncul di folder `database/factories`. Sesuaikan ya dengan nama class model nya.

#### Insert Dengan Factory

Untuk melakukan insert ke database dengan menggunakan factory yaitu dengan.

```php
Product::factory()->create();
```

Jika kalian perlu menambahkan field secara khusus dengan value tertentu. dapat dilakukan seperti ini.

```php
Product::factory()->create([
    "name" => "Produk 12"
]);
```

Jika kalian perlu menginsert beberapa data tanpa harus loop tinggal tambah method `count($count)` setelah factory.

```php
Product::factory()->count(5)->create();
```

### Implementasi Testing

#### Penjelasan Class PassportTestCase

Ini Class yang saya buat dulu untuk Authentication passport dengan bearer token. Jadi Class ini akan menjadi class parent untuk class test seperti `OrderTest`. Cek saja ada pada `extends` dia memanggil `PassportTestCase`. Jadi sebelum memulai test dia akan memanggil method `setUp`. Kita override method nya namun tetap memanggil method `setUp` milik parent. Karena otentikasi memerlukan bearer token maka dibuatlah User dengan factory yang sudah tersedia. Kemudian membuat bearer token disimpan pada header Authorization.

Ada header `Accept` disana agar resultnya dipastikan json saja. Karena ini API.

Kemudian ada juga method method lain itu untuk meng-override method aslinya pada class `TestCase` karena ada header khusus untuk passport authentication.

#### Membuat Testing

Sudah ada contoh testnya di folder `test` yaitu `OrderTest.php` dan `ShipTest.php`. Didalamnya tertuliskan test yang sudah dibuat. Kalian bisa meniru yang didalamnya.

Untuk membuat classnya tinggal dengan command:

```
php artisan make:test OrderTest
```

Untuk membuat satu skenario test kalian tinggal membuat method dengan awalan `test` maka method akan dijalankan. Perhatikan test yang ada pada Class `OrderTest`. Terdapat method `testCreateOrderSuccess` dan `testCreateOrderFailRequest`. Itu adalah test yang dibuat dimana memanggil API save order dengan kondisi sukses dan gagal dikarenakan requestnya kurang (pada testnya tidak dikirim alamat).

Sebelum method test dijalankan, method `setUp` akan dijalankan terlebih dahulu. Itu sudah dari sistem testing nya. Membuat Authorization dll kemudian menjalankan method test nya. Makanya dibuatkan class PassportTestCase sebagai custom TestCase.

Perhatikan terdapat penggunaan trait `DatabaseTransactions` pada bagian awal class.

```php
class OrderTest extends PassportTestCase
{
    use DatabaseTransactions;

```

Code diatas digunakan agar setiap penggunaan CRUD pada database tidak benar benar terjadi. Jadi hanya transaksi saja namun tanpa di commit. (Baca lagi di Database Transaction yaa~).

Dalam membuat test sesuaikan dengan kebutuhan. Seperti yang ada pada save order dia perlu data Product, maka buat saja data product tersebut dengan factory agar lebih cepat. Kalau mau insert dengan cara biasa dengan method `create()` atau `save()` bisa juga namun harus menuliskan field - field lagi pada setiap insert data.

Setelah membuat skenario, pastikan untuk hasilnya harus dicek. Baik result dari json, data pada database, dan class lain agar dipastikan terpanggil.

#### Assertions

Setelah method atau API diakses dengan kondisi tertentu, harus dicek apakah hasilnya sesuai harapan atau tidak. Pada testing akan lebih baik jika hasilnya dicek saat sukses dan juga saat error. Jadi disaat user menggunakan aplikasinya dengan request cukup random, akan menghasilkan error atau sukses.

Untuk hasil dari url HTTP Testing dimana dia mengecek Status Code, JSON result, Session dan lain - lain. Tapi saat ini kita pakai dulu assert Status Code dan assert JSON.

Ada juga diluar HTTP Testing yaitu Database Testing yang dimana dia mengecek apakah field - field pada database itu exist atau tidak. Contoh nya ada pada Test.

```php
$response->assertStatus(201)
    ->assertJson([
        "name" => $this->user->name,
        "user_id" => $this->user->id,
        "address" => $address,
        "total" => $product->price * $qty
    ]);

$this->assertDatabaseHas("orders", [
    "name" => $this->user->name,
    "user_id" => $this->user->id,
    "address" => $address,
    "total" => $product->price * $qty
]);
```

#### Mocking

Sudah dipelajari kan tentang Queue Job, Event dan Listener? Pada saat melakukan test terkadang kita tidak perlu mengecek hasil dari job tersebut. Maka kita lakukan Mocking. Jika Mocking dilakukan, test tidak akan benar - benar memanggil Job, Event atau Listener. Dan bisa dicek juga jika Job tersebut sudah terpanggil atau belum. Tidak hanya class diatas, ada juga Mailer, Notification, Storage dll.

Untuk contoh penggunaan Mock Event nya seperti dibawah.

```php
public function testCreateOrderSuccess()
{
    Event::fake();

    //Skenario Test Dan assert diperlukan


    Event::assertDispatched(OrderCreated::class);
}
```

Jika diperlukan, ada juga mock object jika sebuah class tidak mau benar benar dipanggil jika di Laravel tidak tersedia. Ada contohnya di `ShipTest`.

Namun dalam pendefinisiannya di controller harus seperti ini. `GuzzleHttp\Client` ini class yang dipakai untuk mengakses API 3rd party. Kalian bisa pelajari secara individu ya penggunaan 3rd party.

```php
use GuzzleHttp\Client;

class ShipController extends Controller
{
    private $client;
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    //Ini untuk pemanggilannya
    public function province(Request $r)
    {
        $result = $this->client->get("https://api.rajaongkir.com/starter/province", [
            "headers" => [
                "key" => config("rajaongkir.api_key")
            ]
        ]);
        $provinces = json_decode($result->getBody()->getContents());
        return $provinces->rajaongkir->results;
    }
}
```

Kalau dengan cara biasa seperti dibawah, Class tidak bisa di-mock.

```php
use GuzzleHttp\Client;

class ShipController extends Controller
{
    //Ini untuk pemanggilannya
    public function province(Request $r)
    {
        $client = new Client();
        $result = $client->get("https://api.rajaongkir.com/starter/province", [
            "headers" => [
                "key" => config("rajaongkir.api_key")
            ]
        ]);
        $provinces = json_decode($result->getBody()->getContents());
        return $provinces->rajaongkir->results;
    }
    ...
}
```

Dibawah ini cara melakukan mock nya.

```php
$this->instance(Client::class, Mockery::mock(Client::class, function ($mock) {
    $mock->shouldReceive('get') // Mengecek apakah class memanggil method `get()`
        ->withArgs(function ($url, $options) { // Mengecek apakah method dipanggil dengan parameter tertentu
            return $url === "https://api.rajaongkir.com/starter/province"; // Dicek valuenya. Harus return boolean
        })
        ->once() // Method dipanggil sekali, kalau dipanggil beberapa kali dapat diset `twice()`, `times($count)` dll.
        // Jika method sudah sesuai pemanggilannya, akan menghasilkan objek tertentu.
        // Resultnya sesuaikan bisa saja objek atau integer.
        ->andReturn(new Response(200, [], json_encode([
            "rajaongkir" => [
                "results" => [[
                    "province_id" => 1,
                    "province" => "Provinsi 1"
                ]]
            ]
        ])));
}))->makePartial();
```

Untuk assert nya sudah disatukan dengan cara mock diatas. Jadi dicek berapa kali dipanggil dengan param tertentu. Jika sudah benar maka akan menghasilkan value tertentu.

#### Coverage

Untuk memastikan apakah test sudah tercover ke method dan class yang diperlukan maka harus dicek juga dengan `coverage`. Coba lakukan command dibawah ini untuk mengecek apakah class nya sudah tercover atau belum.

```
php artisan test --coverage
```

atau

```
vendor/bin/phpunit --coverage-text
```

Jika ada error "Tidak ada driver ditemukan." Coba lakukan ya tutorial dari stack overflow ini.

https://stackoverflow.com/a/53284256

Coba run ulang sampai muncul seperti ini.

```
$ vendor/bin/phpunit --coverage-text
PHPUnit 9.5.24

.....                                                               5 / 5 (100%)

Time: 00:02.677, Memory: 46.00 MB

OK (5 tests, 26 assertions)


Code Coverage Report:
  2022-11-05 01:40:37

 Summary:
  Classes:  0.00% (0/3)
  Methods: 41.67% (5/12)
  Lines:   56.34% (40/71)

App\Http\Controllers\Controller
  Methods:  ( 0/ 0)   Lines:  (  0/  0)
App\Http\Controllers\OrderController
  Methods:  66.67% ( 2/ 3)   Lines:  93.75% ( 30/ 32)
App\Http\Controllers\ProductController
  Methods:   0.00% ( 0/ 5)   Lines:   0.00% (  0/ 20)
App\Http\Controllers\ShipController
  Methods:  75.00% ( 3/ 4)   Lines:  52.63% ( 10/ 19)
App\Models\Notification
  Methods:  ( 0/ 0)   Lines:  (  0/  0)
App\Models\Order
  Methods:  ( 0/ 0)   Lines:  (  0/  0)
App\Models\OrderDetail
  Methods:  ( 0/ 0)   Lines:  (  0/  0)
App\Models\Product
  Methods:  ( 0/ 0)   Lines:  (  0/  0)
App\Models\User
  Methods:  ( 0/ 0)   Lines:  (  0/  0)
```

Ada Class, Method dan Line yang tercover dengan satuan persen. Berarti test baru mengcover kode sekian persen.

Jika kalian ingin mengetahui kode yang tercover atau belum tambahkan commandnya seperti ini.

```
vendor/bin/phpunit --coverage-html coverage
```

Ada projek html di folder coverage. Coba buka `index.html`. Disana ada direktori kode class yang sudah tercover atau belum. Kalau test sudah diupdate run ulang saja.

Loh kok gak semua filenya muncul? Ini sudah didefinisikan pada `phpunit.xml`. Sudah saya kurangi yaa~.

```xml
<coverage processUncoveredFiles="true">
    <include>
        <directory suffix=".php">./app/Http/Controllers</directory>
        <directory suffix=".php">./app/Models</directory>
    </include>
</coverage>
```

Itulah sederhananya untuk testing pada Laravel. Testing tidak hanya pada php saja, ada juga di java, python, javascript itu juga baik back-end atau front-end juga. Namun dengan cara yang berbeda.

INGAT Testing hanya dibuat sesuai dengan kebutuhan saja. Biasanya ada pada batasan masalah dari aplikasi yang sudah dibuat.

Referensi:

-   https://laravel.com/docs/9.x/testing#main-content
-   https://laravel.com/docs/9.x/http-tests
-   https://laravel.com/docs/9.x/database-testing
-   https://laravel.com/docs/9.x/mocking#event-fake
-   https://laravel.com/docs/9.x/mocking#queue-fake
-   https://laravel.com/docs/9.x/mocking#mocking-objects
