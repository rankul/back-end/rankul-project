<?php

namespace Tests\Feature;

// use Illuminate\Foundation\Testing\RefreshDatabase;

use App\Events\OrderCreated;
use App\Jobs\InsertNotification;
use App\Models\Product;
use App\Models\User;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Queue;
use Mockery;
use Tests\PassportTestCase;
use Tests\TestCase;

class OrderTest extends PassportTestCase
{
    use DatabaseTransactions;
    /**
     * A basic test example.
     *
     * @return void
     */

    public function testCreateOrderSuccess()
    {
        // Event::fake();
        // Queue::fake();

        $product = Product::factory()->create();
        $address = $this->faker->address;
        $qty = rand(1, 10);
        $response = $this->post('/api/order', [
            "address" => $address,
            "orders" => [
                [
                    "product_id" => $product->id,
                    "quantity" => $qty,
                ]
            ],
        ]);

        $response->assertStatus(201)
            ->assertJson([
                "name" => $this->user->name,
                "user_id" => $this->user->id,
                "address" => $address,
                "total" => $product->price * $qty
            ]);

        $this->assertDatabaseHas("orders", [
            "name" => $this->user->name,
            "user_id" => $this->user->id,
            "address" => $address,
            "total" => $product->price * $qty
        ]);

        $this->assertDatabaseHas("order_details", [
            "name" => $product->name,
            "quantity" => $qty,
            "price" => $product->price,
            "total" => $product->price * $qty,
            "product_id" => $product->id,
        ]);

        $this->assertDatabaseHas("products", [
            "id" => $product->id,
            "stock" => $product->stock - $qty,
        ]);

        // Event::assertDispatched(OrderCreated::class);
        // Queue::assertPushed(InsertNotification::class);
    }

    public function testCreateOrderFailRequest()
    {
        Event::fake();
        Queue::fake();

        $product = Product::factory()->create();
        $address = $this->faker->address;
        $qty = rand(1, 10);
        $response = $this->post('/api/order', [
            // "address" => $address,
            "orders" => [
                [
                    "product_id" => $product->id,
                    "quantity" => $qty,
                ]
            ],
        ]);

        $response->assertStatus(422);

        $this->assertDatabaseMissing("orders", [
            "name" => $this->user->name,
            "user_id" => $this->user->id,
            "address" => $address,
            "total" => $product->price * $qty
        ]);

        $this->assertDatabaseMissing("order_details", [
            "name" => $product->name,
            "quantity" => $qty,
            "price" => $product->price,
            "total" => $product->price * $qty,
            "product_id" => $product->id,
        ]);

        $this->assertDatabaseHas("products", [
            "id" => $product->id,
            "stock" => $product->stock,
        ]);

        Event::assertNotDispatched(OrderCreated::class);
        // Queue::assertPushed(InsertNotification::class);
    }
}
