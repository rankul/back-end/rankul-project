<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;

class ShipController extends Controller
{
    private $client;
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function province(Request $r)
    {
        $result = $this->client->get("https://api.rajaongkir.com/starter/province", [
            "headers" => [
                "key" => config("rajaongkir.api_key")
            ]
        ]);
        $provinces = json_decode($result->getBody()->getContents());
        return $provinces->rajaongkir->results;
    }

    public function city(Request $r)
    {
        $r->validate([
            "province_id" => "required|integer"
        ]);

        $result = $this->client->get("https://api.rajaongkir.com/starter/city?province=" . $r->province_id, [
            "headers" => [
                "key" => config("rajaongkir.api_key")
            ]
        ]);
        $cities = json_decode($result->getBody()->getContents());
        return $cities->rajaongkir->results;
    }

    public function cost(Request $r)
    {
        $r->validate([
            "origin" => "required|integer",
            "destination" => "required|integer",
            "courier" => "required|string"
        ]);

        $result = $this->client->post("https://api.rajaongkir.com/starter/cost", [
            "body" => json_encode([
                "origin" => $r->origin,
                "destination" => $r->destination,
                "weight" => 1,
                "courier" => $r->courier,
            ]),
            "headers" => [
                "key" => config("rajaongkir.api_key"),
                "Content-Type" => "application/json"
            ]
        ]);
        $costs = json_decode($result->getBody()->getContents());
        return $costs->rajaongkir->results;
    }
}
