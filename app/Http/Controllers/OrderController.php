<?php

namespace App\Http\Controllers;

use App\Events\OrderCreated;
use App\Jobs\InsertNotification;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{
    public function __construct()
    {
        $this->middleware("auth:api");
    }

    public function listOrder()
    {
        $data = Order::where("user_id", Auth::user()->id)->get();

        return response()->json($data);
    }

    public function saveOrder(Request $r)
    {
        $r->validate([
            "address" => "required|string",
            "orders" => "required|array",
            "orders.*.product_id" => "required|integer",
            "orders.*.quantity" => "required|integer",
        ]);
        $orderData = DB::transaction(function () use ($r) {
            $product = [];
            $grandTotal = 0;
            foreach ($r->input("orders") as $order) {
                $result = Product::where("stock", ">", $order["quantity"])
                    ->find($order["product_id"]);

                if (!$result) {
                    abort(404, "Produk tidak ditemukan");
                }

                $grandTotal += $result->price * $order["quantity"];
                $result->stock -= $order["quantity"];
                $result->save();
                $product[] = $result;
            }

            $orderData = Order::create([
                "name" => Auth::user()->name,
                "address" => $r->input("address"),
                "total" => $grandTotal,
                "user_id" => Auth::user()->id,
            ]);

            foreach ($product as $key => $item) {
                $quantity = $r->input("orders.{$key}.quantity");
                OrderDetail::create([
                    "name" => $item->name,
                    "quantity" => $quantity,
                    "price" => $item->price,
                    "total" => $item->price * $quantity,
                    "order_id" => $orderData->id,
                    "product_id" => $item->id,
                ]);
            }

            event(new OrderCreated(
                Auth::user()->id,
                "Order #$orderData->id",
                "You create order total Rp. " . $grandTotal
            ));
            return $orderData;
        });


        //201 Status Code : Created
        return response($orderData, 201);
    }
}
