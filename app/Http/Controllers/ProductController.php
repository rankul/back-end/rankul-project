<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware("auth:api");
    }

    // Ini merupakan fungsi untuk mengambil data
    public function listProduct(Request $request)
    {
        $search = $request->input("search");
        $products = Product::all();

        $products = Product::where("name", "like", "%" . $search . "%")
            ->paginate(3);

        return response()->json($products);
    }

    public function detailProduct($product_id)
    {
        $result = Product::find($product_id);
        if (!$result) {
            abort(404, "Produk tidak ditemukan");
        }

        return response()->json($result);
    }

    public function saveProduct(Request $request)
    {
        $request->validate([
            "name" => "required|string",
            "stock" => "required|integer",
            "price" => "required|integer",
        ]);

        $result = new Product();

        if ($request->id) {
            $result = Product::find($request->id);
            if (!$result) {
                abort(404, "Produk tidak ditemukan");
            }
        }
        $result->name = $request->name;
        $result->stock = $request->stock;
        $result->price = $request->price;
        $result->save();

        return response()->json($result);
    }

    public function deleteProduct($product_id = null)
    {
        $result = Product::find($product_id);
        if (!$result) {
            abort(404, "Produk tidak ditemukan");
        }
        $result->delete();

        return response()->json($result);
    }
}
