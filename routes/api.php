<?php

use App\Http\Controllers\OrderController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ShipController;
use App\Http\Middleware\IsActive;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Laravel\Passport\Http\Middleware\CheckClientCredentials;
use Laravel\Passport\Http\Middleware\CheckForAnyScope;
use Laravel\Passport\Http\Middleware\CheckScopes;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware(['auth:api', IsActive::class])->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware(CheckClientCredentials::class)->get('/client_token', function (Request $request) {
    return "client_success";
});

Route::middleware([
    'auth:api', CheckScopes::class . ":get-product"
])->get('/client_scope', function (Request $request) {
    return $request->user();
});

Route::get('/hello', function (Request $request) {
    return "hello world";
});

Route::get("product", ProductController::class . "@listProduct");
Route::get("product/{product_id}", ProductController::class . "@detailProduct");
Route::post("product", ProductController::class . "@saveProduct");
Route::delete("product/{product_id}", ProductController::class . "@deleteProduct");
// Route::put("product", ProductController::class . "@saveProduct");
// Route::patch("product", ProductController::class . "@saveProduct");


Route::post("order", OrderController::class . "@saveOrder");
Route::get("order", OrderController::class . "@listOrder");

Route::get("ship/province", ShipController::class . "@province");
Route::get("ship/city", ShipController::class . "@city");
Route::post("ship/cost", ShipController::class . "@cost");
