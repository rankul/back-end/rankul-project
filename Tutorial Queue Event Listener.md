# Queue Event dan Listener

## Pendahuluan

Terkadang pada saat kalian buat aplikasi ada beberapa proses yang cukup berat contohnya dalam penggunaan 3rd party. Di laravel terdapat fitur yang dinamakan dengan queue. Dengan ini kalian bisa membuat sebuah kegiatan dapat diproses secara background jadi client tak perlu menunggu proses tersebut selesai.

## Pembahasan

Pada materi ini akan dibahas tentang:

-   Queue Job
-   Event Listener

### Queue Job

#### Pendefinisian Queue Job

Rencananya, kalian akan membuat sebuah proses background dimana setiap pembuatan order akan membuat notifikasi. Berikut adalah struktur tabelnya.

tabel `notifications`
|nama_kolom |tipe_data |keterangan |
|-----------|-------------------|-----------|
|id |unsignedBigInteger |primary |
|user_id |unsignedBigInteger |fk_users |
|title |string | |
|message |string | |
|is_read |tinyint(1) |default(0) |
|timestamps |dateTime | |

Sekarang kalian coba buat class job yang akan digunakan untuk insert ke tabel `notifications` dengan menggunakan command.

```
php artisan make:job InsertNotification
```

Maka akan ada file `app/Jobs/InsertNotification.php`. Ada 2 method didalamnya, satu constructor satu lagi `handle()`. Kegunaannya buat apa?

Untuk constructor kalian bisa passing variabel yang akan digunakan. Karena kita butuh `user_id`, `title`, dan `message` buatlah variabel propertinya untuk class dan assignmentnya didalam constructor. Bisa ikuti yang dibawah.

```php
public $user_id, $title, $message;
public function __construct($user_id, $title, $message)
{
    $this->user_id = $user_id;
    $this->title = $title;
    $this->message = $message;
}
```

Tak hanya variabel biasa, kalian juga bisa mengirim objek kedalam constructor. Sesuaikan dengan kebutuhan yaa~

Kemudian di method `handle()` kalian buat proses untuk insert ke tabel `notifications`. Gak dikasih contoh. Yok coba cari tahu gimana cara insert pakai variabel property class.

#### Impementasi Queue Job

Setelah class Job nya sudah siap sekarang bagaimana cara manggil job nya? Coba tambahkan ini setelah melakukan save order.

```php
InsertNotification::dispatch(
    Auth::user()->id,
    "Order #$orderData->id",
    "You create order total Rp. " . $grandTotal
);
```

Parameter di dalam dispatch itu sama dikirim kedalam constructor dari class job nya.

Bisa juga manggil job nya dengan cara ini.

```php
dispatch(new InsertNotification(
    Auth::user()->id,
    "Order #$orderData->id",
    "You create order total Rp. " . $grandTotal
));
```

Coba jalankan save order seharusnya di tabel `notifications` ada data yang diinsert.

Kalau sudah bisa, itu masih secara synchronous. Jadi, client masih menunggu kalau job tersebut berhasil atau tidak. Nah sekarang akan kita buat asynchronous. Gimana caranya?

Coba cek config di `.env` ada `QUEUE_CONNECTION` jadikan `database`.

Kemudian lakukan command dibawah untuk membuat tabel queue. Jangan lupa migrate kalau sudah ada migration nya.

```
php artisan queue:table
```

Agar job berjalan di background lakukan command untuk menjalankan server khusus queue.

```
php artisan queue:work
```

Lakukan lagi save order maka server seharusnya ada output seperti dibawah.

```
$ php artisan queue:work

   INFO  Processing jobs from the [default] queue.

  2022-10-13 12:53:24 App\Jobs\InsertNotification .............................................................................. 33.08ms DONE
```

Jika kalian perlu penanganan dimana method `handle()` ternyata ada `Exception` atau `Throwable` bisa dibuat method `failed()`.

```php
/**
 * Handle a job failure.
 *
 * @param  \Throwable  $exception
 * @return void
 */
public function failed(Throwable $exception)
{
    //Bisa kalian buat notifikasi kalau ternyata job nya error dll
}
```

Kalau ingin jobnya pada saat error ternyata perlu dicoba sebanyak beberapa kali, tambahkan `public $tries=x_kali;` di dalam class nya.

Itulah sederhananya penggunaan queue job. Kalian bisa mempelajarinya lebih lanjut di https://laravel.com/docs/9.x/queues

### Event Listener

#### Pendefinisian Event

Untuk contoh diatas itu masih satu proses pada saat save order. Seandainya kalian perlu kirim email, atau kirim push notification. Masa harus manggil banyak queue job. Disini coba pakai event listener.

Kalau masih bingung coba bayangkan pada saat upacara bendera hari senin. Pada saat pembaca urutan acara mengatakan "Pengibaran bendera merah putih diiringi lagu Indonesia raya". Maka pengibar bendera berformasi untuk mengibarkan bendera, paduan suara menyanyikan lagu Indonesia Raya, marching band memainkan alat musiknya, dan peserta upacara yang lain melakukan sikap hormat kepada bendera merah putih.

Dari ilustrasi diatas `Event` merupakan pembaca urutan acara saat berbicara sedangkan `Listener` adalah pihak yang melakukan kegiatan setelah `Event` tersebut diumumkan.

Pertama kalian coba proses yang sama dahulu dengan job sebelumnya. Yaitu insert notification setelah order berhasil. Coba buat class event `OrderCreated` dahulu dengan command.

```
php artisan make:event OrderCreated
```

Akan ada file `app/Events/OrderCreated.php` dan buatlah constructor sama dengan Job.

#### Pendefinisian Listener

Kemudian buat Listenernya dengan command.

```
php artisan make:listener OrderCreatedInsertNotification --event=OrderCreated
```

Akan ada file `app/Listeners/OrderCreatedInsertNotification.php`. Abaikan bagian constructor atau boleh saja dihapus. dan lanjut ke handle dia ada parameter yang dibutuhkan yaitu `OrderCreated.php`. Agar listener dapat berinteraksi seperti queue. Coba buat listenernya seperti dibawah ini.

```php
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class OrderCreatedInsertNotification implements ShouldQueue
{

    use InteractsWithQueue;

...
```

Coba di dalamnya samakan juga proses insert ke tabel `notifications`.

#### Implementasi Event Listener

Eits belum beres. Sekarang daftarkan event dan listener nya di `app/Providers/EventServiceProvider.php` pada variabel `$listen`

```php
protected $listen = [
    OrderCreated::class => [
        OrderCreatedInsertNotification::class
    ]
];
```

Seandainya ada banyak listener yang mendengarkan sebuah event, tinggal tambah saja. Array-nya bisa dibuat seperti diatas.

Untuk memanggilnya mirip dengan queue job.

```php
OrderCreated::dispatch(
    Auth::user()->id,
    "Order #$orderData->id",
    "You create order total Rp. " . $grandTotal
);
```

```php
event(new OrderCreated(
    Auth::user()->id,
    "Order #$orderData->id",
    "You create order total Rp. " . $grandTotal
));
```

Coba buat order lagi dan akan muncul di server queue nya seperti dibawah.

```
$ php artisan queue:work

   INFO  Processing jobs from the [default] queue.

  2022-10-13 13:20:33 App\Listeners\OrderCreatedInsertNotification ............................................................. 28.44ms DONE
```

Kalian bisa set pada saat failed di Listener nya, dan juga berapa percobaanya sama saja seperti queue job.

Minimal itu saja yang harus kalian bisa tentang Event Listener. Jika kalian mau mempelajari lebih lanjut ada di sini https://laravel.com/docs/9.x/events

Itu saja yang harus kalian pelajari tentang Queue Job, Event dan Listener. Lakukan secara berurut dan cermati cerita - ceritanya.

Referensi:

-   https://laravel.com/docs/9.x/queues#generating-job-classes
-   https://laravel.com/docs/9.x/queues#class-structure
-   https://laravel.com/docs/9.x/queues#dispatching-jobs
-   https://laravel.com/docs/9.x/queues#running-the-queue-worker
-   https://laravel.com/docs/9.x/events#generating-events-and-listeners
-   https://laravel.com/docs/9.x/events#registering-event-subscribers
-   https://laravel.com/docs/9.x/events#dispatching-events
