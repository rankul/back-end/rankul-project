# Eloquent Relationships

## Pendahuluan

Save Order sudah beres dan sudah tahu cara ambil Order milik orang ter*login* kan? Namun tidak hanya Order saja tapi detailnya pun perlu didapatkan. Coba buka file `ERD Rankul.png` disana ada relasi antar entitas yang sudah pernah dibahas pertemuan lalu. Dibawah ini akan dibahas dengan sederhana beberapa penggunaan **_Eloquent Relationships_** pada laravel.

## Pembahasan

Relasi yang ada di gambar `ERD Rankul.png` terdapat:

-   One to Many
-   One to Many (Inverse) / Belongs To
-   One to One

### Pendefinisian _Relationship_

#### One to Many

Pada gambar hubungan antara user dan order adalah One to Many karena seorang user bisa memiliki banyak order dan setiap order memiliki banyak detail pula yang ada. Kalian akan mendefinisikan relasi antara model `Order` dan `OrderDetail`. Bagaimana caranya? perhatikan dibawah ini.

Ini untuk model `Order` karena memiliki banyak `OrderDetail`

```php
use Illuminate\Support\Facades\DB;

class Order extends Model
{
    //Disini banyak variabel apapun yang diperlukan


    //Penulisan relationship itu berupa fungsi, biasanya disimpan paling bawah walau sebenernya bebas.
    //Penulisan nama fungsi bebas asal masuk akal untuk kegunaan tertentu
    public function details()
    {
        return $this->hasMany(OrderDetail::class);
    }

    //Untuk yang diatas secara default dia bakalan nyari kolom sebagai penghubung antara tabel, yaitu `order_id` pada `order_details` sedangkan `id` pada `orders`. Kalian bisa lakukan secara manual jika diperlukan.
    public function details()
    {
        return $this->hasMany(OrderDetail::class, "order_id", "id");
    }
}

```

#### One to Many (Inverse) / Belongs To

Relasi sebelumnya dari `Order` ke `OrderDetail` karena `User` memiliki banyak `Order` maka dapat didefinisikan secara terbalik dengan menggunakan fungsi `belongsTo()`.

```php
use Illuminate\Support\Facades\DB;

class Order extends Model
{
    //idem


    //Idem
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    //Idem
    public function user()
    {
        return $this->belongsTo(User::class, "user_id", "id");
    }
}

```

#### One to One

Relasi satu ke satu pada gambar ERD itu relasi antara `order_details` dan `products`.

```php
use Illuminate\Support\Facades\DB;

class OrderDetail extends Model
{
    //idem


    //Idem
    public function product()
    {
        return $this->hasOne(Product::class);
    }

    //Idem
    public function product()
    {
        return $this->hasOne(Product::class, "product_id", "id");
    }
}

```

### Penggunaan _Relationship_

#### Pengambilan Data Biasa

Setelah relationship sudah didefinisikan, tentunya harus dipakai dongs. Bisa kalian perhatikan dibawah ini bagaimana cara untuk mengambil data yang terhubung dengan relationship. _Check it out~_

```php
$orders = Order::with("details")->where("user_id", Auth::user()->id)->get();

```

Jika data `$orders` dijadikan json maka strukturnya akan seperti dibawah ini:

```json
[
    {
        "id": 1,
        "name": "Test User",
        "address": "Jl. Maharmartanegara SMKN 1 Cimahi",
        "total": 30000,
        "user_id": 1,
        "created_at": "2022-10-01T05:18:31.000000Z",
        "updated_at": "2022-10-01T05:18:31.000000Z",
        "details": [
            {
                "id": 1,
                "name": "Kupat Tahu",
                "quantity": 1,
                "price": 15000,
                "total": 15000,
                "order_id": 1,
                "product_id": 6,
                "created_at": "2022-10-01T05:18:32.000000Z",
                "updated_at": "2022-10-01T05:18:32.000000Z"
            },
            {
                "id": 2,
                "name": "Yahu",
                "quantity": 1,
                "price": 15000,
                "total": 15000,
                "order_id": 1,
                "product_id": 7,
                "created_at": "2022-10-01T05:18:32.000000Z",
                "updated_at": "2022-10-01T05:18:32.000000Z"
            }
        ]
    }
]
```

Penggunaan method `with()` parameternya dapat diisin dengan array of string atau string langsung.

```php
$order = Order::with(["details", "user"])->where("user_id", Auth::user()->id)->get();

```

Tak hanya method `get()` bisa juga pakai `find()`, `findOrfail()` dsb.

```php
$order = Order::with("details")->where("user_id", Auth::user()->id)->findOrFail($order_id);

```

Kalau hanya perlu beberapa kolom saja.

```php
$order = Order::with("details:name,price,quantity,total")->where("user_id", Auth::user()->id)->findOrFail($order_id);

```

Seandainya perlu data yang lebih mendalam karena setiap detail memiliki `product` dapat dituliskan seperti ini.

```php
$orders = Order::with("details.product")->where("user_id", Auth::user()->id)->get();

```

Maka hasilnya akan menjadi seperti ini.

```json
[
    {
        "id": 1,
        "name": "Test User",
        "address": "Jl. Maharmartanegara SMKN 1 Cimahi",
        "total": 30000,
        "user_id": 1,
        "created_at": "2022-10-01T05:18:31.000000Z",
        "updated_at": "2022-10-01T05:18:31.000000Z",
        "details": [
            {
                "id": 1,
                "name": "Kupat Tahu",
                "quantity": 1,
                "price": 15000,
                "total": 15000,
                "order_id": 1,
                "product_id": 6,
                "created_at": "2022-10-01T05:18:32.000000Z",
                "updated_at": "2022-10-01T05:18:32.000000Z",
                "product": {
                    "id": 6,
                    "name": "Kupat Tahu",
                    "stock": 123,
                    "price": 15000,
                    "created_at": "2022-09-10T05:43:35.000000Z",
                    "updated_at": "2022-09-17T04:02:46.000000Z",
                    "deleted_at": null
                }
            },
            {
                "id": 2,
                "name": "Yahu",
                "quantity": 1,
                "price": 15000,
                "total": 15000,
                "order_id": 1,
                "product_id": 7,
                "created_at": "2022-10-01T05:18:32.000000Z",
                "updated_at": "2022-10-01T05:18:32.000000Z",
                "product": {
                    "id": 7,
                    "name": "Yahu",
                    "stock": 100,
                    "price": 15000,
                    "created_at": "2022-09-10T05:44:44.000000Z",
                    "updated_at": "2022-09-17T04:02:46.000000Z",
                    "deleted_at": null
                }
            }
        ]
    }
]
```

Apabila terlalu banyak atau sering dipanggi method `with($relationships)` ini, bisa didefinisikan agar setiap pemanggilan model tersebut sekaligus dengan relationship yang diinginkan dengan cara menambahkan variabel di Model tersebut seperti:

```php
use Illuminate\Support\Facades\DB;

class OrderDetail extends Model
{
    //Pendefinisian variable dll

    protected $with = ["details"];

    //Pendefinisian function method dll
}

```

#### Pengambilan Data Relationship dengan Menggunakan Query

Jika diperlukan seandainya hanya menampilkan `orders` dan `order_details` dengan kondisi tertentu dapat ditambahkan query seperti dibawah.

```php
$product_id = 1000;

$orders = Order::with(["details" => function ($query) use ($product_id) {

    $query->where("product_id", $product_id);

}])->where("user_id", Auth::user()->id)->get();

```

Code diatas dapat dijelaskan dengan ambil semua `orders` yang nilai `user_id` nya orang terlogin beserta `order_details` tetapi untuk data `order_details` hanya yang memiliki `product_id` bernilai `1000`.

#### Pengambilan Data jika Relasi Ditemukan atau Tidak Ada

Terkadang kalian memerlukan sebuah data jika relasinya tidak ditemukan. Misal kalian perlu data `orders` tapi yang hanya memiliki `order_details` saja maka method `whereHas($relationship)` dapat digunakan seperti dibawah.

```php
$orders = Order::whereHas("details")->where("user_id", Auth::user()->id)->get();

```

Tidak hanya `whereHas` tapi bisa menggunakan `orWherehas`.

```php
$orders = Order::orWhereHas("details")->where("user_id", Auth::user()->id)->get();

```

Bisa juga kalian melakukan query juga jika memerlukan kondisi tertentu pada _relationship_.

```php
$product_id = 1000;

$orders = Order::whereHas("details", function ($query) use ($product_id) {

    $query->where("product_id", $product_id);

})->where("user_id", Auth::user()->id)->get();

```

Code diatas dapat dijelaskan dengan ambil semua `orders` yang memiliki `user_id` terlogin jika `order` tersebut memiliki `order_details` dengan nilai `product_id` sama dengan 1000.

Contoh diatas itu untuk mencari apakah relasi ditemukan. Sekarang kalian akan mencoba mencari jika relasi tersebut tidak ada. Perhatikan code dibawah ini.

```php
$orders = Order::whereDoesntHave("details")->where("user_id", Auth::user()->id)->get();

```

Bisa pakai `orWhereDoesntHave($relationship)`.

```php
$orders = Order::orWhereDoesntHave("details")->where("user_id", Auth::user()->id)->get();

```

Bisa juga ditambahkan dengan query tertentu.

```php
$product_id = 1000;

$orders = Order::whereDoesntHave("details", function ($query) use ($product_id) {

    $query->where("product_id", $product_id);

})->where("user_id", Auth::user()->id)->get();

```

#### Gabungan `with` dan `whereHas`

Jika kalian perlu gabungan dari `with` untuk mengambil data dan `whereHas` untuk mengecek relasi ada. Dapat dituliskan dengan:

```php
$product_id = 1000;

$orders = Order::withWhereHas("details", function ($query) use ($product_id) {

    $query->where("product_id", $product_id);

})->where("user_id", Auth::user()->id)->get();

```

Tidak ada method `withWhereDoesntHave` yaa~, ngapain juga ngambil data yang tidak ada wkwk.

#### Update dan Insert pada Relationship

Tak hanya untuk mengambil data, tapi bisa juga untuk input data. Misal, jika diperlukan untuk menambahkan `order_details` tinggal melakukan `save($detail)` untuk data yang akan diinputkan.

Perhatikan code dibawah ini.

```php
$order = Order::find(1);

$detail = new OrderDetail([
    "name" => $name,
    "quantity" => $quantity,
    "price" => $price,
    "total" => $price * $quantity,
    "product_id" => $product_id,
]);

$order->details()->save($detail);

```

Itu untuk satu baris data, sekarang untuk banyak data.

```php
$order = Order::find(1);

$detail[] = new OrderDetail([
    "name" => $name,
    "quantity" => $quantity,
    "price" => $price,
    "total" => $price * $quantity,
    "product_id" => $product_id,
]);

$detail[] = new OrderDetail([
    "name" => $name2,
    "quantity" => $quantity2,
    "price" => $price2,
    "total" => $price2 * $quantity2,
    "product_id" => $product_id2,
]);

$order->details()->saveMany($detail);

```

Bisa juga pakai method `Model::create($attributes)`.

```php
$order = Order::find(1);

$order->details()->create([
    "name" => $name,
    "quantity" => $quantity,
    "price" => $price,
    "total" => $price * $quantity,
    "product_id" => $product_id,
]);

```

Bisa banyak data seperti dibawah

```php
$order = Order::find(1);

$order->details()->createMany([
    [
        "name" => $name,
        "quantity" => $quantity,
        "price" => $price,
        "total" => $price * $quantity,
        "product_id" => $product_id,
    ],
    [
        "name" => $name2,
        "quantity" => $quantity2,
        "price" => $price2,
        "total" => $price2 * $quantity2,
        "product_id" => $product_id2,
    ]
]);

```

INGAT, ini hanya sebagian penjelasan tentang _relationship_. Minimal kalian bisa melakukan tutorial yang diatas jika ternyata diperlukan skill tambahan bisa pelajari secara pribadi di https://laravel.com/docs/9.x/eloquent-relationships

Referensi:

-   https://laravel.com/docs/9.x/eloquent-relationships#one-to-one
-   https://laravel.com/docs/9.x/eloquent-relationships#one-to-many
-   https://laravel.com/docs/9.x/eloquent-relationships#one-to-many-inverse
-   https://laravel.com/docs/9.x/eloquent-relationships#querying-relationship-existence
-   https://laravel.com/docs/9.x/eloquent-relationships#querying-relationship-absence
-   https://laravel.com/docs/9.x/eloquent-relationships#eager-loading
-   https://laravel.com/docs/9.x/eloquent-relationships#constraining-eager-loads-with-relationship-existence
